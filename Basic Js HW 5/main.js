// // Теоретичні питання
// Опишіть своїми словами, що таке метод об'єкту
//Методи об'єкта це функції, які виконуюють певну дію з об'єктом при їх виклику. Ці функції є також пропертіз об'єкту.
// Який тип даних може мати значення властивості об'єкта?
//Ключ об'єкту завжди є стрінгом, але значення ключа може бути: стрінгом, числом, Булєвим типом, масивом, функцією
//або ще одним об'єктом.
// Об'єкт це посилальний тип даних. Що означає це поняття?
//Об'єкт це немутабельний (незмінний) тип данних. Вони можуть містити великий об'єм інформації, тому мають лише ссилку
// на місце в пам'яті, тобто вони не містять значення, а мають лише ссилку на місце в пам'яті, де і знаходяться реальні дані.

// Завдання
// Реалізувати функцію створення об'єкта "юзер".
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
//
// Технічні вимоги:
//     Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера,
// з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin().
// Вивести у консоль результат виконання функції.
//     Необов'язкове завдання підвищеної складності
// Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму.
// Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.
//     Література:
// Об'єкти як асоціативні масиви
// Object.defineProperty()

function createNewUser(firstName, lastName) {
  const NewUser = {
    firstName: firstName,
    lastName: lastName,
    getLogin: function () {
      return `${this.firstName
        .slice(0, 1)
        .toLowerCase()}${this.lastName.toLowerCase()}`;
    },
    setFirstName: function (firstname) {
      this.firstName = firstname;
    },
    setLastName: function (lastName) {
      this.lastName = lastName;
    },
  };
  return NewUser;
}

let UserName = prompt("What's your name?");
let UserLastName = prompt("What's your last name");

let User1 = createNewUser(UserName, UserLastName);
// Object.freeze(User1);
User1.setFirstName("Vitalina");
console.log(User1.firstName);
User1.setLastName("Porada");
console.log(User1.lastName);
console.log(User1.getLogin());
