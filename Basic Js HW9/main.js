// Теоретичні питання
// Опишіть, як можна створити новий HTML тег на сторінці.
//Можна створити за допомогою методу createElement("HTMLtag").
//     Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
//Перший параметр це позицію куди потрібно вставити той чи інший стрінг. 'beforebegin', 'afterbegin', 'beforeend', 'afterend'
//     Як можна видалити елемент зі сторінки?
//За допомогою методу remove() або можна через властивість .innerText = "".
//     Завдання
//     Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку.
//     Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
//
//     Технічні вимоги:
//     Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент,
//     до якого буде прикріплений список (по дефолту має бути document.body.
//     кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
// Приклади масивів, які можна виводити на екран:
//
//     ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ["1", "2", "3", "sea", "user", 23];
// Можна взяти будь-який інший масив.
//     Необов'язкове завдання підвищеної складності:
// Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив,
// виводити його як вкладений список. Приклад такого масиву:
//
//     ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
// Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.
//     Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.
//
//     Література:
// Пошук DOM елементів
// Додавання та видалення вузлів
// Шаблонні рядки
// Array.prototype.map()
// setTimeout, setInterval

const container = document.querySelector(".container");

function showElements(array, container) {
  const generalList = document.createElement("ol");
  generalList.className = "list";
  for (let i = 0; i < array.length; i++) {
    const isInnerList = Array.isArray(array[i]);
    const elem = isInnerList
      ? document.createElement("ol")
      : document.createElement("li");
    if (!isInnerList) {
      elem.innerText = array[i];
    }
    if (isInnerList) {
      for (let element = 0; element < array[i].length; element++) {
        const nestedListItem = document.createElement("li");
        nestedListItem.innerText = array[i][element];
        elem.append(nestedListItem);
      }
    }
    generalList.append(elem);
  }
  container.append(generalList);
}
const cities = [
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin"],
  "Odessa",
  "Lviv",
  "Dnieper",
];

showElements(cities, container);
