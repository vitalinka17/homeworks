// Теоретичні питання
//
// Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
//Нам потрібно екранування для того, щоб програма не розпізнавала якийсь участок коду як код,
// а читало як текст. Наприклад в JS можна екранувати за допомогою " / ".
//
// Які засоби оголошення функцій ви знаєте?
//function declaration: function declaration() {};
//function expression: const expression = function() {return};
//
// Що таке hoisting, як він працює для змінних та функцій?
//Це механізм, коли об'явлення функції та змінних переноситься на початоку зони їх видимості, перед виконанням коду.
//Незалежно, де будуть викликані функції / змінні, вони переносяться наверх зони видимості (незважаючи на те, чи вони
//глобальні або ж локальні).
//
//     Завдання
//     Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем.
//     Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
//
//     Технічні вимоги:
//     Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser())
//     і доповніть її наступним функціоналом:
//     При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
//     Створити метод getAge() який повертатиме скільки користувачеві років.
//     Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі,
//     з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
// Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.
//
function createNewUser(firstName, lastName, birthday) {
  const newUser = {
    firstName: firstName,
    lastName: lastName,
    birthday: birthday,
    getAge: function () {
      return new Date().getFullYear() - new Date(this.birthday).getFullYear();
    },
    getPassword: function () {
      return `${this.firstName
        .slice(0, 1)
        .toUpperCase()}${this.lastName.toLowerCase()}${new Date(
        this.birthday
      ).getFullYear()}`;
    },
    getLogin: function () {
      return `${this.firstName
        .toLowerCase()
        .slice(0, 1)}${this.lastName.toLowerCase()}`;
    },
  };
  return newUser;
}
let userName = prompt("What's your name?");
let userLastName = prompt("What's your last name");
let userBirthday = prompt(
  "When were you born? Input your date of birth in a dd.mm.yy format"
);

userBirthday.split(".");
const date = userBirthday.split(".")[1];
const day = userBirthday.split(".")[0];
const year = userBirthday.split(".")[2];
userBirthday = `${day}.${date}.${year}`;

let user1 = createNewUser(userName, userLastName, year);

console.log(user1.getAge());
console.log(user1.getLogin());
console.log(user1.getPassword());
