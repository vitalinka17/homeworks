// Теоретичні питання
// Опишіть своїми словами що таке Document Object Model (DOM)
//ДОМ - це интерфейс, який дозволяє програмам та скриптам отримати доступ до розітки та його складу, а також
// змінювати структуру, вміст або оформлення.
// Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// Різниця в тому що іннерТекст повертає код розмітки як стрінгу, а іннерХТМЛ повертає тільки контент як стрінг (не у вигляді коду).
//     Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// Можемо звернутися за допомогою двох способів: getElementsBy... та querySelector...(звертаємося за класами СSS) - перший хороший тим,
// що він повертає живу колекцію, а інший статичну колекцію. Я думаю, що спощсіб за кверіСелектором більш вдалий, адже можна більш
// точно назвати який елемент нам потрібен.
//     Завдання
//     Код для завдань лежить в папці project.
//
//     Знайти всі параграфи на сторінці та встановити колір фону #ff0000
//
// Знайти елемент із id="optionsList".
// Вивести у консоль.

// Знайти батьківський елемент та вивести в консоль.
// Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
//
//     Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
//
// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль.
// Кожному з елементів присвоїти новий клас nav-item.


// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
//
//     Література:
// Пошук DOM елементів


// 1)
// const paragraphs = document.querySelectorAll('p')
// for (let paragraph of paragraphs) {
//     paragraph.style.backgroundColor = '#ff0000';
// }

// // 2)
// const optionList = document.getElementById("optionsList");
// console.log(optionList);
//
// // 3)
// const parent = optionList.parentElement;
// console.log(parent);
//
// // 4)
// const childrenNods = optionList.childNodes;
// console.log(childrenNods);
//
// for (let name of childrenNods) {
//     console.log(name.nodeName);
// }
// for (let type of childrenNods) {
//     console.log(type.nodeType);
// }

// 5)
// const testParagraph = document.getElementById('testParagraph');
// testParagraph.innerText = "This is a new paragraph";

// 6)
const mainHeaderElements = document.querySelectorAll('.main-header');
for (let child = 0; child < mainHeaderElements.length; child++) {
    console.log(child);

    // child.classList.add('nav-item');
}

// 7)
// const sectionTitleElements = document.querySelectorAll('.section-title');
// for (let element of sectionTitleElements) {
//     element.classList.remove('section-title');
// }
// console.log(sectionTitleElements);
