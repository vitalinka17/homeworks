// Теоретичні питання
// Описати своїми словами навіщо потрібні функції у програмуванні.
// 1. Фунції потрібні для того, щоб задекларувати або написати якусь операцію; дію або поведінку тому чи іншому стану
// або участку коду. Вона зазвичай пропусується для однієї такої операції, а потім одноразово викликається, виконуючи цю дію,
// не вносячи змін даних / коду.
//     Описати своїми словами, навіщо у функцію передавати аргумент.
// 2. Для того щоб отримати результат функції, адже фунуціЯ може виконувати різні дії, але мати різні значення. ДлЯ того,
// щоб перевикликати функцію з різними значеннями, ми використовуємо різні значення аргументів.
//     Що таке оператор return та як він працює всередині функції?
//Оператор return зупиняє функцію або повертає задане значення, зупиняючи при цьому функцію.
// 3.

//     Завдання

//     Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.
//     Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
//
//     Технічні вимоги:
//     Отримати за допомогою модального вікна браузера два числа.
//     Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати.
//     Сюди може бути введено +, -, *, /.
// Створити функцію, в яку передати два значення та операцію.
//     Вивести у консоль результат виконання функції.
//     Необов'язкове завдання підвищеної складності
// Після введення даних додати перевірку їхньої коректності.
// Якщо користувач не ввів числа, або при вводі вказав не числа,
// - запитати обидва числа знову (при цьому значенням за замовчуванням
//  для кожної зі змінних має бути введена інформація раніше).

let num1 = +prompt("Select your first number");
let num2 = +prompt("Select your second number");
let mathOperation = prompt(
  "Select what you want to do with the chosen numbers!"
);

while (!num1) {
  num1 = +prompt("Select your first number again");
}
while (!num2) {
  num2 = +prompt("Select your second number again");
}

while (
  !(
    mathOperation === "+" ||
    mathOperation === "-" ||
    mathOperation === "*" ||
    mathOperation === "/"
  )
) {
  mathOperation = prompt(
    "Ooops, wrong symbol.( Select what you want to do with the chosen numbers again!"
  );
}
function calculate(num1, num2, callback) {
  if (mathOperation === "+") {
    const add = (num1, num2) => num1 + num2;
    callback = add;
  } else if (mathOperation === "-") {
    const subtractAll = (num1, num2) => num1 - num2;
    callback = subtractAll;
  } else if (mathOperation === "*") {
    const multAll = (num1, num2) => num1 * num2;
    callback = multAll;
  } else if (mathOperation === "/") {
    const divideAll = (num1, num2) => num1 / num2;
    callback = divideAll;
  } else {
    console.log("Unable to count");
  }
  return callback(num1, num2);
}

console.log(calculate(num1, num2, mathOperation));
