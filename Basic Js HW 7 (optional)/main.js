// // Завдання
// // Реалізувати функцію, яка дозволить оцінити,
// // чи команда розробників встигне здати проект до настання дедлайну.
// // Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
// //
// //     Технічні вимоги:
// //     Функція на вхід приймає три параметри: масив із чисел, що становить швидкість роботи різних членів команди.
// //     Кількість елементів у масиві означає кількість людей у команді.
// //     Кожен елемент означає скільки стор поінтів (умовна оцінка складності виконання завдання)
// //     може виконати даний розробник за 1 день.
// //     масив з чисел, який є беклогом (список всіх завдань, які необхідно виконати).
// //     Кількість елементів у масиві означає кількість завдань у беклозі.
// //     Кожне число в масиві означає кількість сторі поінтів, необхідні виконання даного завдання.
// //     Дата дедлайну (об'єк т типу Date).
// // Після виконання, функція повинна порахувати, чи команда розробників встигне
// // виконати всі завдання з беклогу до настання дедлайну (робота ведеться починаючи з сьогоднішнього дня).
// // Якщо так, вивести на екран повідомлення: Усі завдання будуть успішно виконані за ? днів до настання дедлайну!.
// // Підставити потрібну кількість днів у текст. Якщо ні, вивести повідомлення
// // Команді розробників доведеться витратити додатково ? годин після дедлайну, щоб виконати всі завдання в беклозі
// // // Робота триває по 8 годин на день по будніх днях

function showDiff(date1, date2) {
  const diff = Math.abs(Math.floor((date2 - date1) / 1000));

  const days = Math.floor(diff / (24 * 60 * 60));
  const leftSec = diff - days * 24 * 60 * 60;
  const hours = Math.floor(leftSec / (60 * 60));

  return {
    days,
    hours,
  };
}

function timeEvaluation(speedOfWorkers, backlog, deadlineDate) {
  const remainder = showDiff(new Date(), new Date(deadlineDate));
  console.log(remainder);

  const evaluateEffectiveness = (speedOfWorkers) => {
    let accumulator = 0;
    for (let i = 0; i < speedOfWorkers.length; i++) {
      accumulator += speedOfWorkers[i];
    }
    return accumulator;
  };
  const workingWeek = 5;
  const workingHoursPerWeek = 40;
  const workerEfficiency = evaluateEffectiveness(speedOfWorkers) * workingWeek; //65

  const sumPoints = (backlog) => {
    let accumulator = 0;
    for (let i = 0; i < backlog.length; i++) {
      accumulator += backlog[i];
    }
    return accumulator;
  };
  const amountOfTasks = sumPoints(backlog); //640
  const hoursAmountToCompleteTheTask = Math.round(
    (amountOfTasks * workingHoursPerWeek) / workerEfficiency
  );
  const wholeAmountOfHours = remainder.days * 8 + remainder.hours; //600

  if (hoursAmountToCompleteTheTask <= wholeAmountOfHours) {
    console.log(
      `Усі завдання будуть успішно виконані за ${Math.round(
        (wholeAmountOfHours - hoursAmountToCompleteTheTask) / 24
      )} днів до настання дедлайну!`
    );
  } else {
    console.log(
      `Команді розробників доведеться витратити додатково ${
        hoursAmountToCompleteTheTask - wholeAmountOfHours
      } годин після дедлайну, щоб виконати всі завдання в беклозі`
    );
  }
}

const speedOfWorkers = [15, 10, 9];
const backlog = [100, 200, 110];
const deadline = "2022-10-10";

console.log(timeEvaluation(speedOfWorkers, backlog, deadline));
