const changeThemeBtn = document.getElementById("change-theme");
function setTheme(themeName) {
  localStorage.setItem("theme", themeName);
  document.documentElement.className = themeName;
}

function toggleTheme(event) {
  event.preventDefault();
  if (localStorage.getItem("theme") === "dark-theme") {
    setTheme("light-theme");
  } else {
    setTheme("dark-theme");
  }
}
(function () {
  if (localStorage.getItem("theme") === "dark-theme") {
    setTheme("dark-theme");
  } else {
    setTheme("light-theme");
  }
})();
changeThemeBtn.addEventListener("click", toggleTheme);
