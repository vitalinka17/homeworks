// Theoretical questions
// 1. Ми можемо оголосити змінні за допомогою ключових слів: var, const та let.
// 2. Функція prompt вимагає від користувача ввести самостійно інпут за допомогою спеціальної вбудованої форми, значення може бути стрінгом, пустим стрінгом або null. А confirm поп-ап також просить юзера ввести інпут, але повертає значення Boolean типу (ОК або Cancel), на відміну від prompt юзера самостійно вводити нічого не потрібно.
// 3. неявне перетворення типів це по суті автоматичне перетворення типів, коли не було явно вказано який тип данних потрібен. Наприклад, if (...) - аргумент функциї завжди буде булеан типом, '100' - 5 = 95 - стрінг '100' буде автоматично перетворена на число, коли ми будемо віднімати число.

// Task 1
let name = "Vitalina";
let admin = name;
console.log(admin);

// Task 2
let secondsPerDay = 86400;
let days = 5;
let seconds = days * secondsPerDay;
console.log(seconds);

// Task 3
const pollQuestion = prompt("How many litres of beer can you drink per day?");

console.log(pollQuestion);
