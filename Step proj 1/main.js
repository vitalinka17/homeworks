// tabs clicker
const tabs = document.querySelector(".service-items");
const tabsCollection = document.querySelectorAll(".service-items li");
let activeTab = document.querySelector(".service-items .active");
const tabsInfo = document.querySelectorAll(".service-description div");

tabs.addEventListener("click", (event) => {
  tabsCollection.forEach((item) => {
    const activeTab = item.classList.contains("active");
    if (activeTab) {
      item.classList.remove("active");
    }
  });
  let element = event.target;
  element.classList.add("active");
  activeTab = element;
  const id = activeTab.id;
  tabsInfo.forEach((i) => {
    if (i.classList.contains("active-text")) {
      i.classList.remove("active-text");
    }
    if (id === i.className) {
      i.classList.add("active-text");
    }
  });
});
// tabs clicker

// gallery load-more button action
const loadMoreBtn = document.querySelector(".works .load-more");
const images = document.querySelectorAll(".photo-box img");
const loader = document.querySelector(".works .lds-spinner");
let currentItems = 12;

function loadInParts(event) {
  loadMoreBtn.style.display = "none";
  loader.style.display = "inline-block";
  setTimeout(photoUpload, 2000);
  function photoUpload() {
    for (let i = currentItems; i < currentItems + 12; i++) {
      if (images[i]) {
        images[i].style.display = "block";
      }
    }
    currentItems += 12;
    loadMoreBtn.style.display = "inline";
    if (currentItems >= images.length) {
      event.target.style.display = "none";
    }
    loader.style.display = "none";
  }
}
loadMoreBtn.addEventListener("click", loadInParts);

//gallery tabs filter and clicker

const workList = document.querySelector(".work-list");
let activeListItem = document.querySelector(".work-list li.active");
const listItems = document.querySelectorAll(".work-list li");
const allHiddenImages = document.querySelectorAll(".photo-box img.hidden");

workList.addEventListener("click", (event) => {
  if (event.target.tagName.toLowerCase() !== "li") return;
  listItems.forEach((item) => {
    const activeItem = item.classList.contains("active");
    if (activeItem) {
      item.classList.remove("active");
    }
  });
  const targetElement = event.target;
  targetElement.classList.add("active");
  activeListItem = targetElement;
  allHiddenImages.forEach((item) => {
    if (activeListItem.dataset.class === "all") {
      item.style.display = "block";
    }
  });
  images.forEach((image) => {
    if (activeListItem.dataset.class === "all") return;
    activeListItem.dataset.class === image.dataset.class
      ? (image.style.display = "block")
      : (image.style.display = "none");
  });
});
//gallery tabs filter and clicker

//photo slider
const nextButton = document.querySelector(".carousel-photos .right-arrow");
const prevButton = document.querySelector(".carousel-photos .left-arrow");
const photoContainer = document.querySelector(".carousel-photos");
let photoCollection = document.querySelectorAll(".carousel-photos img");
let commentary = document.querySelector(".comment-words");
let reviewerName = document.querySelector(".reviewer");
let reviewerPosition = document.querySelector(".position");
let bigReviewerPhoto = document.querySelector(".reviewer-photo");

const mary = document.getElementById("mary");
const jack = document.getElementById("jack");
const hasan = document.getElementById("hasan");
const kara = document.getElementById("kara");

let currentUser = "mary";
window.addEventListener("load", changePhotoInfo);
const authorComment = {
  mary: "Very good service and friendly atmosphere",
  jack: "Professional and welcoming staff",
  hasan: "Nice conditions for work",
  kara: "Reasonable prices, good value for money",
};
const authorName = {
  mary: "mary o'neil",
  jack: "jack thompson",
  hasan: "hasan ali",
  kara: "kara rogers",
};
const authorPosition = {
  mary: "Power BI",
  jack: "Project Manager",
  hasan: "UX Designer",
  kara: "Front-End Developer",
};

photoCollection = Array.from(photoCollection);
bigReviewerPhoto.style.width = "162px";
bigReviewerPhoto.style.height = "162px";
let index = 0;
bigReviewerPhoto.src = photoCollection[index].src;
function changePhotoInfo() {
  bigReviewerPhoto.src = photoCollection[index].src;
  bigReviewerPhoto.id = photoCollection[index].id;
  currentUser = bigReviewerPhoto.id;
  commentary.innerText = authorComment[currentUser];
  reviewerName.innerText = authorName[currentUser];
  reviewerPosition.innerText = authorPosition[currentUser];
}
function changeTargetedInfo() {
  currentUser = bigReviewerPhoto.id;
  commentary.innerText = authorComment[currentUser];
  reviewerName.innerText = authorName[currentUser];
  reviewerPosition.innerText = authorPosition[currentUser];
}
function nextSlide() {
  index++;
  if (index > photoCollection.length - 1) {
    index = 0;
  }
  changePhotoInfo();
}
function prevSlide() {
  index--;
  if (index < 0) {
    index = photoCollection.length - 1;
  }
  changePhotoInfo();
}

nextButton.addEventListener("click", nextSlide);
prevButton.addEventListener("click", prevSlide);
photoContainer.addEventListener("click", (event) => {
  if (event.target === nextButton || event.target === prevButton) return;
  if (event.target.tagName.toLowerCase() !== "img") return;
  if (event.target) {
    bigReviewerPhoto.src = event.target.src;
    bigReviewerPhoto.id = event.target.id;
    changeTargetedInfo();
  }
});
//photo slider

// load more photos in masonry tiles
const loadMoreGalleryBtn = document.querySelector(".gallery .load-more");
const hiddenImages = document.querySelectorAll(
  ".gallery .img-container.hidden"
);
const spinner = document.querySelector(".gallery .lds-spinner");
function uploadPhotos() {
  loadMoreGalleryBtn.style.display = "none";
  spinner.style.display = "inline-block";
  setTimeout(loadHiddenPhotosMasonry, 2000);
  function loadHiddenPhotosMasonry() {
    hiddenImages.forEach((item) => {
      item.classList.remove("hidden");
    });
    spinner.style.display = "none";
  }
}
loadMoreGalleryBtn.addEventListener("click", uploadPhotos);
// load more photos in masonry tiles
